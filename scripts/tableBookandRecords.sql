CREATE TABLE `books` (
  `idbook` int(11) NOT NULL AUTO_INCREMENT,
  `codBook` varchar(45) DEFAULT NULL,
  `titleBook` varchar(45) DEFAULT NULL,
  `pathBook` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idbook`),
  UNIQUE KEY `idbook_UNIQUE` (`idbook`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

INSERT INTO libraryapi.books (codBook, titleBook, pathBook) values ('CAS', 'Cien Años de Soledad', 'Books/CienAniosDeSoledadPdf.pdf');
INSERT INTO libraryapi.books (codBook, titleBook, pathBook) values ('DAF', 'El Diario de Ana Frank', 'Books/AnaFrankPdf.pdf');
INSERT INTO libraryapi.books (codBook, titleBook, pathBook) values ('AG', 'El Arte de la Guerra', 'Books/ElArteDeLaGuerraPdf.pdf');
INSERT INTO libraryapi.books (codBook, titleBook, pathBook) values ('MPV', 'Marcelino Pan y Vino', 'Books/MarcelinoPanyVinoPdf.pdf');
INSERT INTO libraryapi.books (codBook, titleBook, pathBook) values ('DQM', 'Don Quijote de la Mancha', 'Books/DonQuijotePdf.pdf');

commit;