/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gbh.apibooks.models;

import java.io.Serializable;

/**
 *
 * @author fabian
 */
public class BooksModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer idbook;
    private String codBook;
    private String titleBook;
    private String pathBook;

    public BooksModel() {
    }

    public BooksModel(Integer idbook, String codBook, String titleBook, String pathBook) {
        this.idbook = idbook;
        this.codBook = codBook;
        this.titleBook = titleBook;
        this.pathBook = pathBook;
    } 

    public BooksModel(Integer idbook) {
        this.idbook = idbook;
    }

    public Integer getIdbook() {
        return idbook;
    }

    public void setIdbook(Integer idbook) {
        this.idbook = idbook;
    }

    public String getCodBook() {
        return codBook;
    }

    public void setCodBook(String codBook) {
        this.codBook = codBook;
    }

    public String getTitleBook() {
        return titleBook;
    }

    public void setTitleBook(String titleBook) {
        this.titleBook = titleBook;
    }

    public String getPathBook() {
        return pathBook;
    }

    public void setPathBook(String pathBook) {
        this.pathBook = pathBook;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idbook != null ? idbook.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof BooksModel)) {
            return false;
        }
        BooksModel other = (BooksModel) object;
        if ((this.idbook == null && other.idbook != null) || (this.idbook != null && !this.idbook.equals(other.idbook))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Book: { "
                + "idbook: " + idbook + " , "
                + "codBook: " + codBook + " , "
                + "titleBook: " + titleBook + " , "
                + "pathBook: " + pathBook
                +" }";
    }  
}
