/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gbh.apibooks.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

/**
 *
 * @author fabian
 */
public class ReadBook {
    
    private static final Logger logger = Logger.getLogger(ReadBook.class.getName());
    
    /**
     * This method allows you to read a pdf page by page and return to a page in String format.
     * @param path type String
     * @param page type int
     * @return parsedText type String
     */
    public String readPageBook(String path, Integer page) {

        PDDocument pdDocument = null;
        String parsedText;
        try {
            if (path != null) {
                InputStream file = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                pdDocument = PDDocument.load(file);
                PDFTextStripper pdfStripper = new PDFTextStripper();
                pdfStripper.setStartPage(page);
                pdfStripper.setEndPage(page);
                parsedText = pdfStripper.getText(pdDocument);//new String(.getBytes("Windows-1252"),"UTF-8");
                if (page > pdDocument.getNumberOfPages()) {
                    parsedText = "El libro no tiene mas paginas. La pagina No.".concat(pdDocument.getNumberOfPages() + " es la ultima.");
                }
            } else {
                parsedText = "El libro que intenta consultar no existe, validar ruta: " + path;
            }
        } catch (Exception e) {
            parsedText = "Validar, libro no disponible: " + path;
            logger.log(Level.WARNING,"readPageBook: " + e.getMessage());
        } finally {
            if (pdDocument != null) {
                try {
                    pdDocument.close();
                } catch (IOException e) {
                    parsedText = "No se puedo leer la pagina. ".concat(e.getMessage());
                    logger.log(Level.WARNING,"readPageBook: " + e.getMessage());
                }
            }
        }
        return parsedText;
    }
}
