/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gbh.apibooks.database;

import com.gbh.apibooks.models.BooksModel;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fabian
 */
public class BookDataBase {
    
    private static final Logger logger = Logger.getLogger(BookDataBase.class.getName());
    
    private Connection getConnection() {
        Connection conn = null;
        try {
            Properties p = new Properties();
            p.load(Thread.currentThread().getContextClassLoader().getResource("Properties/connection.properties").openStream());
            conn = DriverManager.getConnection("jdbc:mysql://"
                    .concat(p.getProperty("server"))
                    .concat(":")
                    .concat(p.getProperty("port"))
                    .concat("/")
                    .concat(p.getProperty("db_name"))
                    .concat("?autoReconnect=true&useSSL=false"),
                    p.getProperty("user"),
                    p.getProperty("pass"));
        } catch (SQLException e) {    
            logger.log(Level.SEVERE,"No se puedo conectar a la bbdd: " + e.getMessage());
        } catch (IOException ex) {
            logger.log(Level.SEVERE,"No se puedo conectar a la bbdd: " + ex.getMessage());
        }
        return conn;
    }

    public List<BooksModel> findAllBooks() {

        List<BooksModel> books = new ArrayList<>();        

        try {
            Connection conn = getConnection();
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("SELECT * FROM libraryapi.books");
                ResultSet rs = ps.executeQuery();

                while (rs.next()) {
                    books.add(new BooksModel(rs.getInt("idbook"), 
                            rs.getString("codBook"), 
                            rs.getString("titleBook"), 
                            rs.getString("pathBook")));
                }
                rs.close();
                ps.close();
                conn.close();
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE,"findAllBooks: " + ex.getMessage());
            books = new ArrayList<>();
        }
        return books;
    }

    public BooksModel findBook(Integer id) {

        BooksModel book = new BooksModel();

        try {
            Connection conn = getConnection();

            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("SELECT * FROM libraryapi.books where idbook = ?");
                ps.setInt(1, id);
                ResultSet rs = ps.executeQuery();

                while (rs.next()) {
                    book = new BooksModel(rs.getInt("idbook"), 
                            rs.getString("codBook"), 
                            rs.getString("titleBook"), 
                            rs.getString("pathBook"));
                }
                rs.close();
                ps.close();
                conn.close();
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE,"findBook: " + ex.getMessage());
            book = new BooksModel();
        }
        return book;
    }
}
