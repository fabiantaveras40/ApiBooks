/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gbh.apibooks.rest;

import com.gbh.apibooks.models.BooksModel;
import com.gbh.apibooks.services.BooksServices;
import com.gbh.apibooks.services.IBooks;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author fabian
 */
@Path("/rest/library")
public class BooksApiRest {

    private final IBooks iBooks = new BooksServices();

    /**
     * This method allows the search for a specific book, returned in a Json
     * object. It receives as input parameter a book id.
     * Ej: http://localhost:8080/ApiBooks/api/rest/library/book/1
     * @param id type int
     * @return Book type Json
     */
    @GET
    @Path("/book/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public BooksModel getBook(@PathParam("id") Integer id) {
        return iBooks.find(id);
    }

    /**
     * This method returns a list of available books.
     * Ej: http://localhost:8080/ApiBooks/api/rest/library/books
     * @return book list type Json
     */
    @GET
    @Path("/books")
    @Produces({MediaType.APPLICATION_JSON})
    public List<BooksModel> getBooks() {
        return iBooks.findAll();
    }
}
