/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gbh.apibooks.rest;

import com.gbh.apibooks.services.BooksServices;
import com.gbh.apibooks.services.IBooks;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author fabian
 */
@Path("/rest/book")
public class ReadBooksApiRest {

    private final IBooks iBooks = new BooksServices();

    /**
     * This method allows you to read a pdf page by page and return to a page in
     * String format. Ej: http://localhost:8080/ApiBooks/api/rest/book/1/page/50/text
     *
     * @param id type int
     * @param page type int
     * @return page type String
     */
    @GET
    @Path("/{id}/page/{page}/text")
    @Produces({MediaType.TEXT_PLAIN})
    public String getPageBookText(@PathParam("id") Integer id,
            @PathParam("page") Integer page) {
        return iBooks.pageBook(id, page);
    }

    /**
     * This method allows you to read a pdf page by page and return to a page in
     * html format. Ej: http://localhost:8080/ApiBooks/api/rest/book/1/page/50/html
     *
     * @param id type int
     * @param page type int
     * @return page type html
     */
    @GET
    @Path("/{id}/page/{page}/html")
    @Produces({MediaType.TEXT_HTML})
    public String getPageBookHtml(@PathParam("id") Integer id,
            @PathParam("page") Integer page) {

        return "<html>\n"
                + "\n"
                + "<head>\n"
                + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\n"
                + "</head>\n"
                + "\n"
                + "<body>\n"
                + "<p>" + iBooks.pageBook(id, page) + "</p>\n"
                + "</body>\n"
                + "\n"
                + "</html>";
    }
}
