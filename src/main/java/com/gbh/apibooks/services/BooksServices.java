/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gbh.apibooks.services;

import com.gbh.apibooks.database.BookDataBase;
import com.gbh.apibooks.models.BooksModel;
import com.gbh.apibooks.util.ReadBook;
import java.util.List;

/**
 *
 * @author fabian
 */
public class BooksServices implements IBooks {

    private final BookDataBase bd = new BookDataBase();
    private final ReadBook readBook = new ReadBook();

    /**
     * This method returns a list of available books.
     *
     */
    @Override
    public List<BooksModel> findAll() {
        return bd.findAllBooks();
    }

    /**
     * This method allows the search for a specific book, returned in a Json
     * object. It receives as input parameter a book id.
     *
     * @param id
     */
    @Override
    public BooksModel find(Integer id) {
        return bd.findBook(id);
    }

    /**
     * This method returns a specific page of a book selected by its id. Receive
     * a book id and a page number.
     *
     * @param id
     * @param page
     */
    @Override
    public String pageBook(Integer id, Integer page) {
        return readBook.readPageBook(find(id).getPathBook(), page);
    }
}
