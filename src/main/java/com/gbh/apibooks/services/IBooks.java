/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gbh.apibooks.services;

import com.gbh.apibooks.models.BooksModel;
import java.util.List;

/**
 *
 * @author fabian
 */
public interface IBooks {
    
    public List<BooksModel> findAll();
    
    public BooksModel find(Integer id);
    
    public String pageBook(Integer id, Integer page);
    
}
