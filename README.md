ApiBooks
Version 1.0


Metodos:
-getBook
	*Con este metodo retornamos un libro en base a id.
-getBooks
	*Con este metodo retornamos un lista de libros.
-getPageBookText
	*Con este metodo retornamos un pagina especifica de un libro.
-getPageBookHtml
	*Con este metodo retornamos un pagina especifica de un libro.
	
Encontraran informacion mas detallada en: http://localhost:8080/ApiBooks/apidocs/index.html


Pasos para configurar el proyecto:
	-En la ruta "ApiBooks\src\main\resources\Properties" encontraran un archivo "connection.properties" el 
	mismo contiene los datos para conexion con la base de datos, estos deben ser modificados de acorde a los
	que utilizaran. Ej:
	server=localhost-> Servidor donde se encuentra la base de datos.
	user=root-> Usuario de la base de datos.
	pass=root-> contraseña para la base de datos.
	db_name=libraryapi-> nombre de la base de datos.
	port=3306-> puerto de la base datos.
	Una vez actualizados a los datos de conexion que usaran se podra ejecutar el proyecto.
	
	Nota:Las conexiones a base de datos son recomendables via un datasource que este en el servidor de 
	aplicaciones donde se despligue el servicio, en este prueba estan asi para evitar mas configuraciones del
	lado del servidor.
	
	-En la ruta "ApiBooks\scripts" se encuentra el archivo "tableBookandRecords.sql" el mismo debe ser ejecutado
	en la base de datos previamente seleccionada.
	

Rutas para acceder al servicio:

Este metodo retorna una lista de los libros disponibles:
http://localhost:8080/ApiBooks/api/rest/library/books

Este metodo retorna un libro segun el id enviado, en este caso el 2.
http://localhost:8080/ApiBooks/api/rest/library/book/2

Este metodo retorna la pagina de un libro con un id y numero de pagina, en este caso libro 1 pagina 8.
Formato texto:
http://localhost:8080/ApiBooks/api/rest/book/1/page/8/text
Formato html:
http://localhost:8080/ApiBooks/api/rest/book/1/page/8/html

Entorno utulizado para las pruebas:
-Base de datos MySql.
	*Nombre de la base de datos utilizada: libraryapi
-Servidor de aplicaciones GlassFish.

Nota: Solo existen 5 libros disponibles los mismo se pueden repetir en tantos id como deseen.
	